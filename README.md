# onedrum

LV2 drum sample player.

* One sample.
* One midi note trigger.

Ardour:
![screenshot_ardour](/uploads/47ee6ed4d644700e08695083d496ab0c/2024-01-25-183347_621x212_scrot.png)

Jalv:
![screenshot_jalv](/uploads/7d907a8c9ea0b2680431c45bcbc3a3fb/2024-01-25-183602_640x145_scrot.png)


## Usage

1. Load a sample file.
2. Select midi note trigger (General MIDI).
3. Play some midi into it.

Assemble many instances to make a whole drum kit.
(E.g. have one midi track "send" to many `onedrum` instances.)


## Installation

1. (Install dependencies.)
2. `cd onedrum.lv2`
3. `make`
4. `sudo make install`


## Dependencies

Should be
[lv2](https://gitlab.com/lv2/lv2)
and
[libsndfile](https://github.com/libsndfile/libsndfile)
.

E.g.
```
brew install lv2
brew install libsndfile
```


## License

See the `LICENSE` file.
Applies to whole repo.
