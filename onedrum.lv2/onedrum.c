// See the LICENSE file for how open source this is.

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <lv2/atom/atom.h>
#include <lv2/atom/util.h>
#include <lv2/core/lv2.h>
#include <lv2/core/lv2_util.h>
#include <lv2/midi/midi.h>
#include <lv2/patch/patch.h>
#include <lv2/state/state.h>
#include <lv2/urid/urid.h>

#include <sndfile.h>

#define MYURI "https://myuri/onedrum"
#define MYURI_SAMPLEPATH MYURI "#samplepath"

#define MIN( A, B ) ( ( A ) < ( B ) ? ( A ) : ( B ) )

enum constants {
    MAX_PATH_LENGTH = 1024, // TODO:WARNING what's a good size?
};

enum portindex {
    PORT_AUDIO_OUT_L = 0,
    PORT_AUDIO_OUT_R = 1,
    PORT_ATOM_IN     = 2,
    PORT_ATOM_OUT    = 3,
    PORT_MIDIMAP     = 4,
};

struct instance {
    LV2_Atom_Sequence *port_atom_in;
    LV2_Atom_Sequence *port_atom_out;
    LV2_URID           urid_atomobject;
    LV2_URID           urid_atompath;
    LV2_URID           urid_atomstring;
    LV2_URID           urid_midievent;
    LV2_URID           urid_patchget;
    LV2_URID           urid_patchproperty;
    LV2_URID           urid_patchset;
    LV2_URID           urid_patchvalue;
    LV2_URID           urid_samplepath;
    LV2_URID_Map      *uridmap;
    LV2_URID_Unmap    *uridunmap;
    SF_INFO            snd_info;
    const char        *samplepath;
    float             *port_audio_out_l;
    float             *port_audio_out_r;
    float             *port_midimap;
    float             *sampledata;
    int                note_on;
    size_t             sampleframepos;
};

static void
instantiate_set_urids ( struct instance *instance ) {
    instance->urid_atomobject =
        instance->uridmap->map ( instance->uridmap->handle, LV2_ATOM__Object );
    instance->urid_atompath =
        instance->uridmap->map ( instance->uridmap->handle, LV2_ATOM__Path );
    instance->urid_atomstring =
        instance->uridmap->map ( instance->uridmap->handle, LV2_ATOM__String );
    instance->urid_midievent =
        instance->uridmap->map ( instance->uridmap->handle, LV2_MIDI__MidiEvent );
    instance->urid_patchget =
        instance->uridmap->map ( instance->uridmap->handle, LV2_PATCH__Get );
    instance->urid_patchproperty =
        instance->uridmap->map ( instance->uridmap->handle, LV2_PATCH__property );
    instance->urid_patchset =
        instance->uridmap->map ( instance->uridmap->handle, LV2_PATCH__Set );
    instance->urid_patchvalue =
        instance->uridmap->map ( instance->uridmap->handle, LV2_PATCH__value );
    instance->urid_samplepath =
        instance->uridmap->map ( instance->uridmap->handle, MYURI_SAMPLEPATH );
}

static const char *
instantiate_get_features_check_missing (
    const LV2_Feature *const *features, struct instance *instance
) {
    const char *missing = lv2_features_query (
        features,
        LV2_URID__map,
        &instance->uridmap,
        true,
        LV2_URID__unmap,
        &instance->uridunmap,
        true,
        0
    );

    return missing;
}

static LV2_Handle
instantiate (
    const LV2_Descriptor     *descriptor,
    double                    sample_rate,
    const char               *bundle_path,
    const LV2_Feature *const *features
) {
    struct instance *instance;

    (void)descriptor;
    (void)sample_rate;
    (void)bundle_path;

    instance = calloc ( 1, sizeof ( struct instance ) );

    const char *missing = instantiate_get_features_check_missing ( features, instance );
    if ( missing ) {
        free ( instance );
        return 0;
    }

    instantiate_set_urids ( instance );

    return instance;
}

static void
connect_port ( LV2_Handle handle_of_instance, uint32_t port, void *data_location ) {
    struct instance *instance = handle_of_instance;

    switch ( port ) {
    case PORT_AUDIO_OUT_L:
        instance->port_audio_out_l = data_location;
        return;
    case PORT_AUDIO_OUT_R:
        instance->port_audio_out_r = data_location;
        return;
    case PORT_ATOM_IN:
        instance->port_atom_in = data_location;
        return;
    case PORT_ATOM_OUT:
        instance->port_atom_out = data_location;
        return;
    case PORT_MIDIMAP:
        instance->port_midimap = data_location;
        return;
    }

    printf (
        "ONEDRUM: error: unknown port connection: %d\n", port
    ); // TODO:WARNING Proper way to handle?
}

static void
activate ( LV2_Handle instance ) {
    (void)instance;
}

static size_t
write_sampledata_get_frame_stop (
    size_t frame_start, uint32_t frames, size_t frame_max
) {
    return MIN ( frame_start + frames, frame_max );
}

static void
write_sampledata_close (
    size_t frame_stop, struct instance *instance, size_t idx_sample
) {
    size_t channels  = instance->snd_info.channels;
    size_t frame_max = instance->snd_info.frames;

    instance->sampleframepos = frame_stop;

    if ( ( frame_max - channels ) < idx_sample ) {
        // TODO:INFO this is not the place (function) to set this
        instance->note_on = 0;
    }

    // TODO:INFO this refactoring is shit.
}

static void
write_audio_out_write_sampledata (
    uint32_t offset, struct instance *instance, uint32_t frames
) {
    size_t channels    = instance->snd_info.channels;
    size_t frame_start = instance->sampleframepos;
    size_t frame_max   = instance->snd_info.frames;
    size_t frame_stop =
        write_sampledata_get_frame_stop ( frame_start, frames, frame_max );
    size_t idx_port   = 0;
    size_t idx_sample = frame_start;

    for ( ; ( idx_port < frames ) && ( idx_sample < frame_stop ); ) {
        instance->port_audio_out_l[offset + idx_port] =
            instance->sampledata[idx_sample * channels];

        instance->port_audio_out_r[offset + idx_port] =
            instance->sampledata[idx_sample * channels + ( channels - 1 )];

        idx_port++;
        idx_sample++;

        // TODO:INFO could use memcpy
    }

    write_sampledata_close ( frame_stop, instance, idx_sample );
}

static void
write_audio_out ( struct instance *instance, uint32_t offset, uint32_t frames ) {
    if ( instance->note_on && instance->sampledata ) {
        write_audio_out_write_sampledata ( offset, instance, frames );
    } else {
        memset ( instance->port_audio_out_l + offset, 0, frames * sizeof ( float ) );
        memset ( instance->port_audio_out_r + offset, 0, frames * sizeof ( float ) );
        // TODO:INFO No "sizeof(float)", use sizeof first element.
    }
}

static void
handle_midievent ( struct instance *instance, LV2_Atom_Event *event ) {
    uint8_t *midi_msg  = (uint8_t *)( event + 1 );
    int      msg_type  = lv2_midi_message_type ( midi_msg );
    uint8_t  midi_note = midi_msg[1];
    // TODO:INFO uint8_t  midi_vel  = midi_msg[2];

    if ( msg_type == LV2_MIDI_MSG_NOTE_ON ) {
        if ( midi_note == ( uint8_t ) * ( instance->port_midimap ) ) {
            instance->note_on        = 1; // TODO:INFO can do this differently?
            instance->sampleframepos = 0;
        }
    }
}

static void *
load_sample_realloc_sampledata ( struct instance *instance ) {
    return realloc (
        instance->sampledata,
        sizeof ( float ) * instance->snd_info.frames * instance->snd_info.channels
    );
}

static void
load_sample_close ( struct instance *instance, SNDFILE *snd_file ) {
    sf_close ( snd_file );

    printf (
        "ONEDRUM: load_sample(): samplepath: %s\n", instance->samplepath
    ); // TODO:INFO Remove?

    printf (
        "ONEDRUM: load_sample(): channels: %d\n", instance->snd_info.channels
    ); // TODO:INFO Remove?
}

static void
load_sample_read_file ( struct instance *instance, SNDFILE *snd_file ) {
    sf_read_float (
        snd_file,
        instance->sampledata,
        instance->snd_info.frames * instance->snd_info.channels
    );
    // TODO:WARNING convert sample rate
}

static void
load_sample ( struct instance *instance ) {
    SNDFILE *snd_file = { 0 };

    snd_file = sf_open ( instance->samplepath, SFM_READ, &instance->snd_info );
    if ( !snd_file ) {
        printf ( "ONEDRUM: error: failed to open sample file\n"
        ); // TODO:INFO Proper way to handle?
        return;
    }

    instance->sampledata = load_sample_realloc_sampledata ( instance );
    if ( !instance->sampledata ) {
        printf ( "ONEDRUM: load_sample(): realloc failed\n"
        ); // TODO:INFO Proper logging?
        return;
    }

    load_sample_read_file ( instance, snd_file );

    load_sample_close ( instance, snd_file );
}

static int
patchset_receive_is_good_property (
    struct instance *instance, LV2_Atom_URID *msg_property
) {
    if ( !msg_property ) {
        printf ( "ONEDRUM: error: msg property is null\n"
        ); // TODO:INFO Proper way to handle?
        return 0;
    }
    if ( msg_property->body != instance->urid_samplepath ) {
        printf (
            "ONEDRUM: error: unknown property: %s\n", // TODO:INFO Proper way to handle?
            instance->uridunmap->unmap (
                instance->uridunmap->handle, msg_property->body
            )
        );
        return 0;
    }

    return 1;
}

static int
patchset_receive_is_good_value ( struct instance *instance, LV2_Atom *msg_value ) {
    if ( !msg_value ) {
        printf ( "ONEDRUM: error: msg value is null\n"
        ); // TODO:INFO Proper way to handle?
        return 0;
    }
    if ( msg_value->type != instance->urid_atompath ) {
        printf ( "ONEDRUM: error: value type is not path\n"
        ); // TODO:INFO Proper way to handle?
        return 0;
    }

    return 1;
}

static void
patchset_receive ( struct instance *instance, LV2_Atom_Object *atom_obj ) {
    LV2_Atom_URID *msg_property = { 0 };
    LV2_Atom      *msg_value    = { 0 };
    int            is_good_property;
    int            is_good_value;

    lv2_atom_object_get (
        atom_obj,
        instance->urid_patchproperty,
        (LV2_Atom **)&msg_property,
        instance->urid_patchvalue,
        &msg_value,
        0
    );
    is_good_property = patchset_receive_is_good_property ( instance, msg_property );
    is_good_value    = patchset_receive_is_good_value ( instance, msg_value );
    if ( !is_good_property || !is_good_value ) {
        return;
    }

    instance->samplepath = LV2_ATOM_BODY ( msg_value );
    instance->samplepath = strndup ( instance->samplepath, MAX_PATH_LENGTH );
    printf ( "ONEDRUM: patchset_receive(): samplepath '%s'\n", instance->samplepath );
    load_sample ( instance ); // TODO:INFO worker thread?
}

static void
handle_atomobject ( struct instance *instance, LV2_Atom_Event *event ) {
    LV2_Atom_Object *atom_obj = (LV2_Atom_Object *)&event->body;
    uint32_t         obj_type = atom_obj->body.otype;

    if ( obj_type == instance->urid_patchget ) {
        printf ( "ONEDRUM: handle_atomobject(): lv2patchget\n"
        ); // TODO:ERROR Implement.
    } else if ( obj_type == instance->urid_patchset ) {
        patchset_receive ( instance, atom_obj );
    } else {
        printf (
            "ONEDRUM: error: unknown object type: %s\n",
            instance->uridunmap->unmap ( instance->uridunmap->handle, obj_type )
        ); // TODO:INFO Proper way to handle?
    }
}

static void
handle_atom_in ( struct instance *instance, LV2_Atom_Event *event ) {
    uint32_t atom_type = event->body.type;

    if ( atom_type == instance->urid_midievent ) {
        handle_midievent ( instance, event );
    } else if ( atom_type == instance->urid_atomobject ) {
        handle_atomobject ( instance, event );
    } else {
        printf (
            "ONEDRUM: error: unknown atom type: %s\n",
            instance->uridunmap->unmap ( instance->uridunmap->handle, event->body.type )
        ); // TODO:INFO Proper way to handle?
    }
}

static void
run ( LV2_Handle instance, uint32_t sample_count ) {
    struct instance *inst   = instance;
    uint32_t         offset = 0;

    LV2_ATOM_SEQUENCE_FOREACH ( inst->port_atom_in, ev ) {
        handle_atom_in ( inst, ev );
        write_audio_out ( inst, offset, ev->time.frames - offset );
        offset = ev->time.frames;
    }

    write_audio_out ( inst, offset, sample_count - offset );
}

static void
deactivate ( LV2_Handle instance ) {
    (void)instance;
}

static void
cleanup ( LV2_Handle instance ) {
    free ( instance );
}

static void
save_do_store (
    struct instance         *instance,
    LV2_State_Handle         handle_of_state,
    LV2_State_Store_Function store
) {
    size_t path_size;

    path_size = strnlen ( instance->samplepath, MAX_PATH_LENGTH );

    store (
        handle_of_state,
        instance->urid_samplepath,
        instance->samplepath,
        path_size,
        instance->urid_atomstring,
        ( LV2_STATE_IS_POD | LV2_STATE_IS_PORTABLE )
    );
}

static LV2_State_Status
save (
    LV2_Handle                handle_of_instance,
    LV2_State_Store_Function  store,
    LV2_State_Handle          handle_of_state,
    uint32_t                  flags,
    const LV2_Feature *const *features
) {
    struct instance *instance = handle_of_instance;

    (void)flags;
    (void)features;

    printf ( "ONEDRUM: save(): save the state\n" ); // TODO:INFO Remove?
    printf (
        "ONEDRUM: save(): samplepath = '%s'\n", instance->samplepath
    ); // TODO:INFO Remove?

    if ( !instance->samplepath ) {
        return LV2_STATE_SUCCESS; // TODO:INFO Is this a success?
        // TODO:INFO Really nothing to save if no sample path?
    }

    save_do_store ( instance, handle_of_state, store );

    return LV2_STATE_SUCCESS;
}

static const void *
restore_do_retrieve (
    struct instance            *instance,
    LV2_State_Retrieve_Function retrieve,
    LV2_State_Handle            handle_of_state
) {
    const void *pointer_of_retrieve;
    LV2_URID    urid_type_of_value;

    pointer_of_retrieve = retrieve (
        handle_of_state, instance->urid_samplepath, 0, &urid_type_of_value, 0
    );

    if ( urid_type_of_value != instance->urid_atomstring ) {
        printf ( "ONEDRUM: error: restore(): unexpected type\n"
        ); // TODO:INFO Proper way to handle?
        return 0;
    }
    if ( !pointer_of_retrieve ) {
        printf ( "ONEDRUM: error: restore(): nothing retrieved\n"
        ); // TODO:INFO Proper way to handle?
        return 0;
    }

    return pointer_of_retrieve;
}

static LV2_State_Status
restore (
    LV2_Handle                  handle_of_instance,
    LV2_State_Retrieve_Function retrieve,
    LV2_State_Handle            handle_of_state,
    uint32_t                    flags,
    const LV2_Feature *const   *features
) {
    struct instance *instance = handle_of_instance;
    const void      *pointer_of_retrieve;

    (void)flags;
    (void)features;

    printf ( "ONEDRUM: restore(): \n" ); // TODO:INFO Remove?

    pointer_of_retrieve = restore_do_retrieve ( instance, retrieve, handle_of_state );
    if ( !pointer_of_retrieve ) {
        return LV2_STATE_SUCCESS; // TODO:INFO not "success"?
    }

    instance->samplepath = strndup ( pointer_of_retrieve, MAX_PATH_LENGTH );
    printf ( "ONEDRUM: restore(): samplepath = '%s'\n", instance->samplepath );
    load_sample ( instance ); // TODO:INFO worker thread?

    return LV2_STATE_SUCCESS;
}

static const LV2_State_Interface state = { save, restore };

static const void *
extension_data ( const char *uri ) {
    if ( !strcmp ( uri, LV2_STATE__interface ) ) {
        return &state;
    }

    return 0;
}

static const LV2_Descriptor descriptor = {
    MYURI,
    instantiate,
    connect_port,
    activate,
    run,
    deactivate,
    cleanup,
    extension_data };

LV2_SYMBOL_EXPORT const LV2_Descriptor *
lv2_descriptor ( uint32_t index ) {
    if ( index == 0 ) {
        return &descriptor;
    }

    return 0;
}
