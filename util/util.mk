f: format
format:
	clang-format -i --style="file:${TOP}/util/style.clang-format" ./*.c

l: lint
lint:
	clang-tidy ./*.c --fix --config-file='${TOP}/util/lint.clang-tidy'
