#include "lv2/core/lv2.h"

#include <stdint.h>
#include <stdlib.h>

#define MYURI "https://myuri/randnoise"

enum portnums {
  OUTPUT = 0,
};

struct instance {
  float *output;
};

static LV2_Handle
instantiate(
  const LV2_Descriptor     *descriptor,
  double                    sample_rate,
  const char               *bundle_path,
  const LV2_Feature* const *features)
{
  struct instance *inst = calloc(1, sizeof(struct instance));

  return inst;
}

static void
connect_port(LV2_Handle instance, uint32_t port, void *data_location)
{
  struct instance *inst = instance;

  if (port == OUTPUT) {
    inst->output = data_location;
  }
}

static void
activate(LV2_Handle instance)
{
}

static void
run(LV2_Handle instance, uint32_t sample_count)
{
  struct instance *inst = instance;

  for (size_t i = 0; i < sample_count; i++) {
    inst->output[i] = (rand() * 1.0) / RAND_MAX;
  }
}

static void
deactivate(LV2_Handle instance)
{
}

static void
cleanup(LV2_Handle instance)
{
  free(instance);
}

static const void*
extension_data(const char *uri)
{
  return 0;
}

static const LV2_Descriptor descriptor = {
  MYURI,
  instantiate,
  connect_port,
  activate,
  run,
  deactivate,
  cleanup,
  extension_data
};

LV2_SYMBOL_EXPORT const LV2_Descriptor*
lv2_descriptor(uint32_t index)
{
  if (index == 0) {
    return &descriptor;
  } else {
    return 0;
  }
}
