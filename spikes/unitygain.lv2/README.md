This plugin is a [spike](https://wiki.c2.com/?SpikeDescribed) trial experiment.
It simply has 1 input and 1 output audio port.
It copies all input to the output, unchanged.
