#include "lv2/atom/atom.h"
#include "lv2/atom/util.h"
#include "lv2/core/lv2.h"
#include "lv2/core/lv2_util.h"
#include "lv2/midi/midi.h"
#include "lv2/urid/urid.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MYURI "https://myuri/midinoise"

enum portindex {
  PORT_CTRL = 0,
  PORT_OUT  = 1,
};

struct instance {
  const LV2_Atom_Sequence *control;
  float                   *out;
  LV2_URID_Map            *map;
  LV2_URID                 midievent;
  int                      on;
};

static LV2_Handle
instantiate(
  const LV2_Descriptor     *descriptor,
  double                    sample_rate,
  const char               *bundle_path,
  const LV2_Feature* const *features)
{
  struct instance *inst;

  (void)descriptor;
  (void)sample_rate;
  (void)bundle_path;

  inst = calloc(1, sizeof(struct instance));
  const char *missing = lv2_features_query(
    features,
    LV2_URID__map, &inst->map, true,
    NULL);

  if (missing) {
    free(inst);
    return NULL;
  }

  inst->midievent = inst->map->map(inst->map->handle, LV2_MIDI__MidiEvent);

  return inst;
}

static void
connect_port(LV2_Handle instance, uint32_t port, void *data_location)
{
  struct instance *inst = instance;

  if (port == PORT_CTRL) {
    inst->control = data_location;
  } else if (port == PORT_OUT) {
    inst->out = data_location;
  }
}

static void
activate(LV2_Handle instance)
{
  (void)instance;
}

static void
writeout(struct instance *inst, uint32_t offset, uint32_t len)
{
  if (inst->on) {
    for (size_t i = 0; i < len; i++) {
      inst->out[offset + i] = (rand() * 1.0) / RAND_MAX;
    }
  } else {
    memset(inst->out + offset, 0, len * sizeof(float));
  }
}

static void
run(LV2_Handle instance, uint32_t sample_count)
{
  struct instance *inst   = instance;
  uint32_t         offset = 0;

  LV2_ATOM_SEQUENCE_FOREACH (inst->control, ev) {
    if (ev->body.type == inst->midievent) {
      uint8_t *msg     = (uint8_t *)(ev + 1);
      int      msgtype = lv2_midi_message_type(msg);

      if (msgtype == LV2_MIDI_MSG_NOTE_ON) {
        inst->on = 1;
      } else if (msgtype == LV2_MIDI_MSG_NOTE_OFF) {
        inst->on = 0;
      }
    }

    writeout(inst, offset, ev->time.frames - offset);
    offset = ev->time.frames;
  }

  writeout(inst, offset, sample_count - offset);
}

static void
deactivate(LV2_Handle instance)
{
  (void)instance;
}

static void
cleanup(LV2_Handle instance)
{
  free(instance);
}

static const void*
extension_data(const char *uri)
{
  (void)uri;

  return 0;
}

static const LV2_Descriptor descriptor = {
  MYURI,
  instantiate,
  connect_port,
  activate,
  run,
  deactivate,
  cleanup,
  extension_data
};

LV2_SYMBOL_EXPORT const LV2_Descriptor*
lv2_descriptor(uint32_t index)
{
  if (index == 0) {
    return &descriptor;
  } else {
    return 0;
  }
}
