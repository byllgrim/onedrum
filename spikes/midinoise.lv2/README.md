This plugin is a [spike](https://wiki.c2.com/?SpikeDescribed) trial experiment.
It has 1 atom input port (midi events), and 1 audio output port.
When midi "on" events occur, it produces random noise.
When midi "off" events occur, it stops producing random noise.
