This plugin is a [spike](https://wiki.c2.com/?SpikeDescribed) trial experiment.
It has 1 input and 1 output atom port ("patch:Message" type).
It also has 1 parameter ("atom:path" type).
When a "patch:Get" message is received, it sends a "patch:Set" to specify a
path value for its path parameter.
