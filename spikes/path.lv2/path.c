#include <stdio.h>
#include <stdlib.h>

#include <lv2/atom/atom.h>
#include <lv2/atom/forge.h>
#include <lv2/atom/util.h>
#include <lv2/core/lv2.h>
#include <lv2/core/lv2_util.h>
#include <lv2/patch/patch.h>
#include <lv2/urid/urid.h>

#define MYURI "https://myuri/path"
#define URI_PARAMPATH MYURI "#path"

struct instance {
    const LV2_Atom_Sequence *port_ctrl;
    const LV2_Atom_Sequence *port_notify;
    LV2_URID_Map            *uridmap;
    LV2_URID                 urid_atompath;
    LV2_URID                 urid_atomurid;
    LV2_URID                 urid_patchget;
    LV2_URID                 urid_patchset;
    LV2_URID                 urid_patchproperty;
    LV2_URID                 urid_patchvalue;
    LV2_URID                 urid_parampath;
    LV2_Atom_Forge           forge;
};

void
populate_uridmap ( struct instance *inst ) {
    inst->urid_atompath = inst->uridmap->map ( inst->uridmap->handle, LV2_ATOM__Path );
    inst->urid_atomurid = inst->uridmap->map ( inst->uridmap->handle, LV2_ATOM__URID );
    inst->urid_patchget = inst->uridmap->map ( inst->uridmap->handle, LV2_PATCH__Get );
    inst->urid_patchset = inst->uridmap->map ( inst->uridmap->handle, LV2_PATCH__Set );
    inst->urid_patchproperty =
        inst->uridmap->map ( inst->uridmap->handle, LV2_PATCH__property );
    inst->urid_patchvalue =
        inst->uridmap->map ( inst->uridmap->handle, LV2_PATCH__value );
    inst->urid_parampath = inst->uridmap->map ( inst->uridmap->handle, URI_PARAMPATH );
}

static LV2_Handle
instantiate (
    const LV2_Descriptor     *descriptor,
    double                    sample_rate,
    const char               *bundle_path,
    const LV2_Feature *const *features
) {
    struct instance *inst;
    const char      *missing;

    (void)descriptor;
    (void)sample_rate;
    (void)bundle_path;

    printf ( "INSTANTIATE\n" );

    inst = calloc ( 1, sizeof ( struct instance ) );

    missing = lv2_features_query ( features, LV2_URID__map, &inst->uridmap, true, 0 );
    if ( missing ) {
        free ( inst );
        return 0;
    }

    populate_uridmap ( inst );
    lv2_atom_forge_init ( &inst->forge, inst->uridmap );

    return inst;
}

static void
connect_port ( LV2_Handle instance, uint32_t port, void *data_location ) {
    struct instance *inst = instance;

    printf ( "CONNECT_PORT\n" );

    if ( port == 0 ) {
        inst->port_ctrl = data_location;
    }
    if ( port == 1 ) {
        inst->port_notify = data_location;
    }
}

static void
activate ( LV2_Handle instance ) {
    (void)instance;

    printf ( "ACTIVATE\n" );
}

void
send_patch_set_message ( struct instance *inst ) {
    LV2_Atom_Forge_Frame frame;

    lv2_atom_forge_frame_time ( &inst->forge, 0 /*inst->frame_offset*/ );
    lv2_atom_forge_object ( &inst->forge, &frame, 0, inst->urid_patchset );

    lv2_atom_forge_key ( &inst->forge, inst->urid_patchproperty );
    lv2_atom_forge_urid ( &inst->forge, inst->urid_parampath );

    lv2_atom_forge_key ( &inst->forge, inst->urid_patchvalue );
    lv2_atom_forge_path (
        &inst->forge,
        "/home/user/src/njsb/samples/kick/70s-drum-kick.wav",
        strlen ( "/home/user/src/njsb/samples/kick/70s-drum-kick.wav" )
    );

    lv2_atom_forge_pop ( &inst->forge, &frame );
}

int
is_property_parampath_urid ( struct instance *inst, const LV2_Atom_URID *property ) {
    if ( !property ) {
        printf ( "NO PROPERTY\n" );
        return 0;
    }

    if ( property->atom.type != inst->urid_atomurid ) {
        printf ( "PROPERTY NOT URID\n" );
        return 0;
    }

    if ( property->body != inst->urid_parampath ) {
        printf ( "PROPERTY URID NOT PARAMPATH\n" );
        return 0;
    }

    return 1;
}

int
is_value_path_string ( struct instance *inst, const LV2_Atom_String *value ) {
    if ( !value ) {
        printf ( "NO VALUE\n" );
        return 0;
    }

    if ( value->atom.type != inst->urid_atompath ) {
        printf ( "NOT A PATH VALUE\n" );
    }

    return 1;
}

void
handle_patch_set_value ( const LV2_Atom_String *set_msg_value ) {
    char *byte_pointer;
    char *value_string;

    byte_pointer = (char *)set_msg_value;
    value_string = byte_pointer + sizeof ( set_msg_value->atom );

    printf ( "PATH SET TO '%s'\n", value_string );
}

void
analyze_patch_set_message ( struct instance *inst, const LV2_Atom_Object *obj ) {
    const LV2_Atom_URID   *set_msg_property = 0;
    const LV2_Atom_String *set_msg_value    = 0;

    printf ( "RECEIVED PATCH SET MESSAGE\n" );

    lv2_atom_object_get (
        obj,
        inst->urid_patchproperty,
        (const LV2_Atom **)&set_msg_property,
        inst->urid_patchvalue,
        &set_msg_value,
        0
    );

    if ( !is_property_parampath_urid ( inst, set_msg_property ) ) {
        return;
    }

    if ( !is_value_path_string ( inst, set_msg_value ) ) {
        return;
    }

    handle_patch_set_value ( set_msg_value );
}

void
dispatch_atom_object ( struct instance *inst, const LV2_Atom_Object *obj ) {
    if ( obj->body.otype == inst->urid_patchget ) {
        printf ( "RECEIVED PATCH GET MESSAGE\n" );
        send_patch_set_message ( inst );
        return;
    }

    if ( obj->body.otype == inst->urid_patchset ) {
        analyze_patch_set_message ( inst, obj );
        return;
    }

    printf ( "RECEIVED AN UNKNOWN EVENT\n" );
}

static void
run ( LV2_Handle instance, uint32_t sample_count ) {
    struct instance       *inst;
    const LV2_Atom_Object *obj;
    uint32_t               notify_capacity;
    LV2_Atom_Forge_Frame   out_frame;

    (void)sample_count;

    inst = instance;

    notify_capacity = inst->port_notify->atom.size;
    lv2_atom_forge_set_buffer (
        &inst->forge, (uint8_t *)inst->port_notify, notify_capacity
    );
    lv2_atom_forge_sequence_head ( &inst->forge, &out_frame, 0 );

    LV2_ATOM_SEQUENCE_FOREACH ( inst->port_ctrl, event ) {
        obj = (const LV2_Atom_Object *)&event->body;
        dispatch_atom_object ( inst, obj );
    }

    lv2_atom_forge_pop ( &inst->forge, &out_frame );
}

static void
deactivate ( LV2_Handle instance ) {
    (void)instance;

    printf ( "DEACTIVATE\n" );
}

static void
cleanup ( LV2_Handle instance ) {
    free ( instance );

    printf ( "CLEANUP\n" );
}

static const void *
extension_data ( const char *uri ) {
    (void)uri;

    return 0;
}

static const LV2_Descriptor descriptor = {
    MYURI,
    instantiate,
    connect_port,
    activate,
    run,
    deactivate,
    cleanup,
    extension_data };

LV2_SYMBOL_EXPORT const LV2_Descriptor *
lv2_descriptor ( uint32_t index ) {
    if ( index == 0 ) {
        return &descriptor;
    }

    return 0;
}
