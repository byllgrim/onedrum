#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lv2/atom/atom.h"
#include <lv2/atom/forge.h>
#include "lv2/atom/util.h"
#include "lv2/core/lv2.h"
#include "lv2/patch/patch.h"
#include "lv2/state/state.h"
#include "lv2/urid/urid.h"

#define  MYURI                 "https://myuri/state"
#define  MYURI_PARAMFREQUENCY  MYURI "#frequency"

enum {
  PORT_ATOM_IN  = 0,
  PORT_ATOM_OUT = 1
};

struct instance {
  LV2_Atom_Forge     atomforge;
  LV2_Atom_Sequence *atomport_in;
  LV2_Atom_Sequence *atomport_out;
  LV2_URID           urid_lv2atomfloat;
  LV2_URID           urid_lv2atomurid;
  LV2_URID           urid_lv2patchget;
  LV2_URID           urid_lv2patchproperty;
  LV2_URID           urid_lv2patchset;
  LV2_URID           urid_lv2patchvalue;
  LV2_URID           urid_paramfrequency;
  LV2_URID_Map      *uridmap;
  LV2_URID_Unmap    *uridunmap;
  float              param_frequency;
};

static LV2_Handle
instantiate(
  const LV2_Descriptor     *descriptor,
  double                    sample_rate,
  const char               *bundle_path,
  const LV2_Feature* const *features)
{
  struct instance           *inst;
  const LV2_Feature* const  *feat;
  const char                *uri;

  (void) descriptor;
  (void) sample_rate;
  (void) bundle_path;

  inst = calloc(1, sizeof(struct instance));

  for (feat = features; *feat; feat++) {
    uri = (*feat)->URI;
    if (strcmp(uri, LV2_URID__map) == 0) {
      inst->uridmap = (*feat)->data;
    }
    if (strcmp(uri, LV2_URID__unmap) == 0) {
      inst->uridunmap = (*feat)->data;
    }
  }

  inst->urid_lv2atomurid      = inst->uridmap->map(inst->uridmap->handle, LV2_ATOM__URID);
  inst->urid_lv2atomfloat     = inst->uridmap->map(inst->uridmap->handle, LV2_ATOM__Float);
  inst->urid_lv2patchget      = inst->uridmap->map(inst->uridmap->handle, LV2_PATCH__Get);
  inst->urid_lv2patchproperty = inst->uridmap->map(inst->uridmap->handle, LV2_PATCH__property);
  inst->urid_lv2patchvalue    = inst->uridmap->map(inst->uridmap->handle, LV2_PATCH__value);
  inst->urid_lv2patchset      = inst->uridmap->map(inst->uridmap->handle, LV2_PATCH__Set);
  inst->urid_paramfrequency   = inst->uridmap->map(inst->uridmap->handle, MYURI_PARAMFREQUENCY);

  lv2_atom_forge_init(&inst->atomforge, inst->uridmap);

  return inst;
}

static void
connect_port(LV2_Handle instance_handle, uint32_t port, void *data_location)
{
  struct instance *instance = (struct instance *)instance_handle;

  if (port == PORT_ATOM_IN) {
    instance->atomport_in = data_location;
    return;
  }
  if (port == PORT_ATOM_OUT) {
    instance->atomport_out = data_location;
    return;
  }
  printf(MYURI ": warning: port unknown\n");
}

static void
activate(LV2_Handle instance)
{
  (void)instance;
}

static void
send_patchset(struct instance *instance)
{
  LV2_Atom_Forge_Frame  frame;
  LV2_Atom_Forge       *atomforge             = &instance->atomforge;
  LV2_URID              urid_lv2patchset      = instance->urid_lv2patchset;
  LV2_URID              urid_lv2patchproperty = instance->urid_lv2patchproperty;
  LV2_URID              urid_lv2patchvalue    = instance->urid_lv2patchvalue;
  LV2_URID              urid_paramfrequency   = instance->urid_paramfrequency;
  float                 param_frequency       = instance->param_frequency;

  printf("TODO 'send_patchset() %f'\n", param_frequency);

  if (!atomforge) {
    printf(MYURI ": error: no atom forge\n");
    return;
  }
  lv2_atom_forge_frame_time(atomforge, 0 /*instance->frame_offset*/);
  lv2_atom_forge_object(atomforge, &frame, 0, urid_lv2patchset);

  lv2_atom_forge_key(atomforge, urid_lv2patchproperty);
  lv2_atom_forge_urid(atomforge, urid_paramfrequency);

  lv2_atom_forge_key(atomforge, urid_lv2patchvalue);
  lv2_atom_forge_float(atomforge, param_frequency);

  lv2_atom_forge_pop(atomforge, &frame);
}

static LV2_URID
get_patchproperty_urid(struct instance *instance, const LV2_Atom_Object *atom_object)
{
  LV2_Atom      *atom                  = {0};
  LV2_Atom_URID *atom_urid             = {0};
  LV2_URID       urid_lv2atomurid      = instance->urid_lv2atomurid;
  LV2_URID       urid_lv2patchproperty = instance->urid_lv2patchproperty;

  lv2_atom_object_get(atom_object, urid_lv2patchproperty, &atom, 0);

  if (!atom) {
    printf(MYURI ": error: get_patchproperty_urid: no property\n");
    return 0;  // Assuming 0 is not any useful urid
  }
  if (atom->type != urid_lv2atomurid) {
    printf(MYURI ": error: get_patchproperty_urid: not urid atom\n");
    return 0;  // Assuming 0 is not any useful urid
  }

  atom_urid = (LV2_Atom_URID *)atom;

  return  atom_urid->body;
}

static float
get_patchvalue_float(struct instance *instance, const LV2_Atom_Object *atom_object)
{
  LV2_Atom       *atom               = {0};
  LV2_Atom_Float *atom_float         = {0};
  LV2_URID        urid_lv2atomfloat  = instance->urid_lv2atomfloat;
  LV2_URID        urid_lv2patchvalue = instance->urid_lv2patchvalue;

  lv2_atom_object_get(atom_object, urid_lv2patchvalue, &atom, 0);
  if (!atom) {
    printf(MYURI ": error: get_patchvalue_float: no value\n");
    return 0;  // Assuming 0 is not any useful urid
  }
  if (atom->type != urid_lv2atomfloat) {
    printf(MYURI ": error: get_patchvalue_float: not float atom\n");
    return 0;  // Assuming 0 is not any useful urid
  }

  atom_float = (LV2_Atom_Float *)atom;

  return  atom_float->body;
}

static void
receive_patchset(struct instance *instance, const LV2_Atom_Object *atom_object)
{
  LV2_URID urid_paramfrequency   = instance->urid_paramfrequency;
  LV2_URID urid_patchsetproperty = {0};
  float    patchsetvalue         = {0};

  urid_patchsetproperty = get_patchproperty_urid(instance, atom_object);

  if (urid_patchsetproperty == urid_paramfrequency) {
    patchsetvalue = get_patchvalue_float(instance, atom_object);
    instance->param_frequency = patchsetvalue;
  }
}

static void
run(LV2_Handle instance_handle, uint32_t sample_count)
{
  LV2_Atom_Forge       *atomforge;
  LV2_Atom_Forge_Frame  forgeframe;
  LV2_Atom_Object      *atomobject;
  size_t                size_forgebuffer;
  struct instance      *instance;
  uint32_t              object_type;
  uint8_t              *forgebuffer;

  (void)sample_count;

  instance = (struct instance *)instance_handle;
  size_forgebuffer = instance->atomport_out->atom.size;
  atomforge = &instance->atomforge;
  forgebuffer = (uint8_t *)instance->atomport_out;

  lv2_atom_forge_set_buffer(atomforge, forgebuffer, size_forgebuffer);  // TODO why must this be in "run()"?
  lv2_atom_forge_sequence_head(atomforge, &forgeframe, 0);  // TODO why must this be in "run()"?

  LV2_ATOM_SEQUENCE_FOREACH (instance->atomport_in, event) {
    atomobject = (LV2_Atom_Object *)&event->body; /*how to know it is castable?*/
    object_type = atomobject->body.otype;

    if (object_type == instance->urid_lv2patchget) {
 printf("TODO patchget '%d'\n", atomobject->body.otype);
 printf("TODO patchget '%s'\n", instance->uridunmap->unmap(instance->uridunmap->handle, atomobject->body.otype));
      send_patchset(instance);
    } else if (object_type == instance->urid_lv2patchset) {
      receive_patchset(instance, atomobject);
    } else {
      printf("TODO unknown atom object type: %d\n", object_type);
    }
  }

  lv2_atom_forge_pop(atomforge, &forgeframe);  // TODO why must this be in "run()"?
}

static void
deactivate(LV2_Handle instance)
{
  (void)instance;
}

static void
cleanup(LV2_Handle instance)
{
  free(instance);
}

static LV2_State_Status
save(
  LV2_Handle                 instance_handle,
  LV2_State_Store_Function   store,
  LV2_State_Handle           handle,
  uint32_t                   flags,
  const LV2_Feature *const  *features)
{
  struct instance  *instance     = (struct instance *)instance_handle;
  uint32_t          storekey     = instance->urid_paramfrequency;
  float             storevalue   = instance->param_frequency;
  size_t            storesize    = sizeof(instance->param_frequency);
  uint32_t          storetype    = instance->urid_lv2atomfloat;
  LV2_State_Status  state_status = {0};

  (void) features;

  printf("TODO 'save()'\n");

  flags |= (LV2_STATE_IS_POD | LV2_STATE_IS_PORTABLE);
  state_status = store(handle, storekey, &storevalue, storesize, storetype, flags);
  printf("TODO storing param frequency: %f\n", storevalue);

  return  state_status;
}

static LV2_State_Status
restore(
  LV2_Handle                   instance_handle,
  LV2_State_Retrieve_Function  retrieve,
  LV2_State_Handle             handle,
  uint32_t                     flags,
  const LV2_Feature *const    *features)
{
  struct instance *instance            = (struct instance *)instance_handle;
  LV2_URID         urid_lv2atomfloat   = instance->urid_lv2atomfloat;
  const char      *retrievevalue       = {0};
  size_t           retrievesize        = {0};
  size_t           size_paramfrequency = sizeof(instance->param_frequency);
  uint32_t         retrievekey         = instance->urid_paramfrequency;
  uint32_t         retrievetype        = {0};

  (void)features;

  printf("TODO 'restore()'\n");

  retrievevalue = retrieve(handle, retrievekey, &retrievesize, &retrievetype, &flags);
  if (!retrievevalue) {
    printf("TODO no retrieve value\n");
    return LV2_STATE_SUCCESS;
  }
  if (retrievesize != size_paramfrequency) {
    printf("TODO bad retrieve size\n");
  }
  if (retrievetype != urid_lv2atomfloat) {
    printf("TODO bad retrieve type\n");
  }

  instance->param_frequency = *((float *)retrievevalue);
  printf("TODO retrieving param frequency: %f\n", instance->param_frequency);

  return LV2_STATE_SUCCESS;
}

static const LV2_State_Interface  state = {
  save,
  restore
};

static const void*
extension_data(const char *uri)
{
  if (!strcmp(uri, LV2_STATE__interface)) {
    return &state;
  }

  return 0;
}

static const LV2_Descriptor  descriptor = {
  MYURI,
  instantiate,
  connect_port,
  activate,
  run,
  deactivate,
  cleanup,
  extension_data
};

LV2_SYMBOL_EXPORT const LV2_Descriptor*
lv2_descriptor(uint32_t index)
{
  if (index == 0) {
    return &descriptor;
  } else {
    return 0;
  }
}
