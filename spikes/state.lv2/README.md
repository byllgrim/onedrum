This plugin is a [spike](https://wiki.c2.com/?SpikeDescribed) trial experiment.
It has a float param, saves it and restores it.

Note: Works with "jalv" but not "ardour", because ardour has weird
idiosyncrasies were any non-path parameter _needs_ to have a control,
or something like that.
